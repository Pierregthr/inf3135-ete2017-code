#include <stdio.h>

unsigned int longueur(char chaine[]) {
    unsigned int i = 0;
    while (chaine[i] != '\0') {
        ++i;
    }
    return i;
}

int main() {
    char s[] = "tomate";
    printf("La longueur de \"%s\" est %d\n", s, longueur(s));
    return 0;
}
